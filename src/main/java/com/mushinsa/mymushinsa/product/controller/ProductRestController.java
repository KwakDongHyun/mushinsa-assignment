package com.mushinsa.mymushinsa.product.controller;

import com.mushinsa.mymushinsa.message.ResponseMessage;
import com.mushinsa.mymushinsa.product.model.ProductParam;
import com.mushinsa.mymushinsa.product.model.ProductDto;
import com.mushinsa.mymushinsa.product.repository.ProductRepository;
import com.mushinsa.mymushinsa.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.Charset;

/**
 * @author      : kwakdonghyun
 * @description : ProductRestController
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/product")
public class ProductRestController {

    private final ProductService productService;

    private final ProductRepository productRepository;

    @GetMapping("/all-product/min-price")
    public ResponseEntity<ResponseMessage> getProductListByMinPrice() {

        ProductDto productDto = productService.getProductListByMinPrice();
        ResponseMessage message = new ResponseMessage();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        if(productDto != null) {
            message.setStatus("" + HttpStatus.OK.value());
            message.setMessage("데이터를 정상적으로 수신하였습니다.");
            message.setData(productDto);
        } else {
            message.setStatus("" + HttpStatus.NO_CONTENT.value());
            message.setMessage("" + HttpStatus.NO_CONTENT.getReasonPhrase());
        }

        return new ResponseEntity<>(message, headers, HttpStatus.OK);
    }

    @GetMapping("/brand/min-price")
    public ResponseEntity<ResponseMessage> getBrandByMinPrice() {

        ProductDto productDto = productService.getBrandByMinPrice();
        ResponseMessage message = new ResponseMessage();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        if(productDto != null) {
            message.setStatus("" + HttpStatus.OK.value());
            message.setMessage("데이터를 정상적으로 수신하였습니다.");
            message.setData(productDto);
        } else {
            message.setStatus("" + HttpStatus.NO_CONTENT.value());
            message.setMessage("" + HttpStatus.NO_CONTENT.getReasonPhrase());
        }

        return new ResponseEntity<>(message, headers, HttpStatus.OK);
    }

    @GetMapping("/category/{category}/min-max-price")
    public ResponseEntity<ResponseMessage> getProductByMinMaxPrice(@PathVariable("category") int category) {

        ProductParam param = ProductParam.builder()
                .category(category)
                .build();

        ProductDto productDto = productService.getProductByMinMaxPrice(param);
        ResponseMessage message = new ResponseMessage();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        if(productDto != null) {
            message.setStatus("" + HttpStatus.OK.value());
            message.setMessage("데이터를 정상적으로 수신하였습니다.");
            message.setData(productDto);
        } else {
            message.setStatus("" + HttpStatus.NO_CONTENT.value());
            message.setMessage("" + HttpStatus.NO_CONTENT.getReasonPhrase());
        }

        return new ResponseEntity<>(message, headers, HttpStatus.OK);
    }

}
