package com.mushinsa.mymushinsa.product.service;

import com.mushinsa.mymushinsa.code.entity.CodeDetail;
import com.mushinsa.mymushinsa.code.repository.CodeDetailRepository;
import com.mushinsa.mymushinsa.product.entity.Product;
import com.mushinsa.mymushinsa.product.model.ProductDto;
import com.mushinsa.mymushinsa.product.model.ProductParam;
import com.mushinsa.mymushinsa.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author      : kwakdonghyun
 * @description : ProductService
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    private final CodeDetailRepository codeDetailRepository;

    public ProductDto getProductListByMinPrice() {

        List<CodeDetail> codeDetailList = codeDetailRepository.findByMasterCodeId(100);
        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "price");
        ProductDto productDto = new ProductDto();
        List<ProductParam> productList = new ArrayList<>();
        productDto.setProductList(productList);

        int totalPrice = 0;

        for(CodeDetail codeDetail : codeDetailList) {
            Product product = productRepository.findFirst1ByCategory(codeDetail.getDetailCodeId(), Sort.by(order));
            totalPrice += product.getPrice();

            ProductParam productParam = ProductParam.builder()
                            .brand(product.getBrand())
                            .category(product.getCategory())
                            .categoryName(codeDetail.getDetailCodeNm())
                            .price(product.getPrice())
                            .build();

            productDto.getProductList().add(productParam);
        }

        productDto.setTotalPrice(totalPrice);

        return productDto;
    }

    public ProductDto getBrandByMinPrice() {

        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "brand");
        List<Product> productList = productRepository.findAll(Sort.by(order));
        Map<String, Integer> brandPriceMap = new HashMap<>();
        int totalPrice = 0;

        for(Product product : productList) {
            String brand = product.getBrand();

            if(brandPriceMap.containsKey(brand)) {
                totalPrice = brandPriceMap.get(brand) + product.getPrice();
            } else {
                totalPrice = product.getPrice();
            }

            brandPriceMap.put(brand, totalPrice);
        }

        int minPrice = 0;
        String brand = null;
        for(String key : brandPriceMap.keySet()) {
            if(brand == null || minPrice > brandPriceMap.get(key)) {
                minPrice = brandPriceMap.get(key);
                brand = key;
            }
        }

        ProductDto productDto = new ProductDto();
        ProductParam productParam = ProductParam.builder()
                .price(minPrice)
                .brand(brand)
                .build();

        productDto.setMinProduct(productParam);

        return productDto;
    }

    public ProductDto getProductByMinMaxPrice(ProductParam param) {

        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "price");
        List<Product> productList = productRepository.findByCategory(param.getCategory(), Sort.by(order));

        ProductParam minProductParam = ProductParam.builder()
                .brand(productList.get(0).getBrand())
                .price(productList.get(0).getPrice())
                .build();

        ProductParam maxProductParam = ProductParam.builder()
                .brand(productList.get(productList.size()-1).getBrand())
                .price(productList.get(productList.size()-1).getPrice())
                .build();

        ProductDto productDto = new ProductDto();
        productDto.setMaxProduct(maxProductParam);
        productDto.setMinProduct(minProductParam);

        return productDto;
    }

}
