package com.mushinsa.mymushinsa.product.entity;

import lombok.*;

import javax.persistence.*;

/**
 * @author      : kwakdonghyun
 * @description : Product
 */
@Entity
@Table(name="PRODUCT")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private int id;

    @Column(name="BRAND")
    private String brand;

    @Column(name="CATEGORY")
    private int category;

    @Column(name="PRICE")
    private int price;

}
