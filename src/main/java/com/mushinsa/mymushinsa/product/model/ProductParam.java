package com.mushinsa.mymushinsa.product.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

/**
 * @author      : kwakdonghyun
 * @description : ProductParam
 */
@Data
@Builder
public class ProductParam {

    private String brand;
    private int category;
    private String categoryName;
    private int price;

}
