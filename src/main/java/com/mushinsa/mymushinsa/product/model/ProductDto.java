package com.mushinsa.mymushinsa.product.model;

import lombok.Builder;
import lombok.Data;
import java.util.List;

/**
 * @author      : kwakdonghyun
 * @description : ProductResponse
 */
@Data
public class ProductDto {

    List<ProductParam> productList;     // 다중 상품 리스트
    private ProductParam minProduct;    // 최소 가격 상품
    private ProductParam maxProduct;    // 최대 가격 상품
    private int totalPrice;             // 총 가격

}
