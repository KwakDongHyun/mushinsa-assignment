package com.mushinsa.mymushinsa.product.repository;

import com.mushinsa.mymushinsa.product.entity.Product;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author      : kwakdonghyun
 * @description : ProductRepository
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findFirst1ByCategory(int category, Sort sort);

    List<Product> findByCategory(int category);

    List<Product> findByCategory(int category, Sort sort);
}
