package com.mushinsa.mymushinsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MymushinsaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MymushinsaApplication.class, args);
	}

}
