package com.mushinsa.mymushinsa.code.repository;

import com.mushinsa.mymushinsa.code.entity.CodeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author      : kwakdonghyun
 * @description : CodeDetailRepository
 */
@Repository
public interface CodeDetailRepository extends JpaRepository<CodeDetail, Integer> {

    List<CodeDetail> findByMasterCodeId(int masterCodeId);
}
