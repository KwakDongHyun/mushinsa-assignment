package com.mushinsa.mymushinsa.code.entity;

import lombok.*;

import javax.persistence.*;

/**
 * @author      : kwakdonghyun
 * @description : CodeDetail
 */
@Entity
@Table(name="code_detail")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CodeDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="DETAIL_CODE_ID")
    private int detailCodeId;

    @Column(name="MASTER_CODE_ID")
    private int masterCodeId;

    @Column(name="DETAIL_CODE_NM")
    private String detailCodeNm;

    @Column(name="USE_FL")
    private String useFlag;

}
